CREATE TABLE `Comuni`
(
  `id` integer PRIMARY KEY,
  `comune` varchar(255),
  `istat` integer,
  `provincia` varchar(255),
  `prov` varchar(255),
  `regione` varchar(255),
  `pop_residente` integer,
  `densita_demogr` integer,
  `superficie_kmq` integer,
  `grado_urbaniz` varchar(255),
  `latitudione_g1` integer,
  `latitudione_g2` integer,
  `longitudine_g1` integer,
  `longitudine_g2` integer
);

CREATE TABLE `Emergenze`
(
  `ID` integer PRIMARY KEY,
  `ID_localita` integer,
  `latitudine` float,
  `longitudine` float,
  `tipo` varchar(255),
  `grado` integer,
  `data_inizio` datetime,
  `data_fine` datetime,
  `attiva` boolean
);

CREATE TABLE `Interventi`
(
  `ID` integer PRIMARY KEY,
  `ID_emergenza` integer,
  `ID_utente` integer,
  `accettato` boolean
);

CREATE TABLE `Login`
(
  `ID` integer PRIMARY KEY,
  `telefono` integer,
  `username` varchar(255),
  `password` varchar(255)
);

CREATE TABLE `Squadre`
(
  `ID` integer PRIMARY KEY,
  `nome` varchar(255),
  `ID_caposquadra` integer,
  `ID_volontario` integer
);

CREATE TABLE `Volontari`
(
  `ID` integer PRIMARY KEY,
  `nome` varchar(255),
  `cognome` varchar(255),
  `caposquadra` boolean,
  `ID_squadra` integer,
  `ID_localita` integer,
  `disponibilita` tinyint(2),
  `token` varchar(255),
  `latitudine` float,
  `longitudine` float
);

ALTER TABLE `Login` ADD FOREIGN KEY (`ID`) REFERENCES `Volontari` (`ID`);

ALTER TABLE `Volontari` ADD FOREIGN KEY (`ID`) REFERENCES `Squadre` (`ID_caposquadra`);

ALTER TABLE `Volontari` ADD FOREIGN KEY (`ID`) REFERENCES `Squadre` (`ID_volontario`);

ALTER TABLE `Emergenze` ADD FOREIGN KEY (`ID`) REFERENCES `Interventi` (`ID_emergenza`);

ALTER TABLE `Volontari` ADD FOREIGN KEY (`ID`) REFERENCES `Interventi` (`ID_utente`);

ALTER TABLE `Volontari` ADD FOREIGN KEY (`ID_squadra`) REFERENCES `Squadre` (`ID`);

ALTER TABLE `Comuni` ADD FOREIGN KEY (`id`) REFERENCES `Volontari` (`ID_localita`);

ALTER TABLE `Comuni` ADD FOREIGN KEY (`id`) REFERENCES `Emergenze` (`ID_localita`);
